import 'package:flutter/material.dart';
import 'package:flutter_mat_splash_screen/screens/home_screen.dart';
import 'package:flutter_mat_splash_screen/screens/onboarding_screen.dart';
import 'package:flutter_mat_splash_screen/screens/splash_screen.dart';

var routes = <String, WidgetBuilder>{
  "/home": (BuildContext context) => HomeScreen(),
  "/onboard": (BuildContext context) => OnboardingScreen(),
};

void main() => runApp(new MaterialApp(
    theme:
    ThemeData(primaryColor: Colors.red, accentColor: Colors.yellowAccent),
    debugShowCheckedModeBanner: false,
    home: SplashScreen(),
    routes: routes)
);